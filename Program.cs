﻿using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Web.Services;
using TestWebMethod.BLL;

namespace TestWebMethod
{
    class Program
    {
        private readonly ILogger _logger;
        private readonly OrderBLL _orderBLL;

        public Program(ILogger logger, OrderBLL orderBLL)
        {
            _logger = logger;
            _orderBLL = orderBLL;
        }

        [WebMethod]
        public OrderAnswer LoadOrderInfo(string orderCode)
        {
            OrderAnswer orderAnswer = new OrderAnswer();

            if (string.IsNullOrEmpty(orderCode))
            {
                orderAnswer.Status = OrderStatus.Error;
                orderAnswer.ErrorMessage = "Ordercode is null or empty";

                return orderAnswer;
            }

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            try
            {
                orderAnswer.Order = _orderBLL.GetOrderFromCache(orderCode);
                if (orderAnswer.Order is null)
                {
                    orderAnswer.Status = OrderStatus.NotFound;
                    orderAnswer.ErrorMessage = "Order not found";
                    return orderAnswer;
                }

                orderAnswer.Status = OrderStatus.Found;
            }
            catch (Exception ex)
            {
                string errorMessage = "Failed to get information about order";
                _logger.Log("ERROR", errorMessage);

                orderAnswer.Status = OrderStatus.Error;
                orderAnswer.ErrorMessage = errorMessage;
            }
            finally
            {
                stopWatch.Stop();
                _logger.Log("INFO", "Elapsed - {0}", stopWatch.Elapsed);
            }

            return orderAnswer;
        }
    }
}
