﻿namespace TestWebMethod
{
    public enum OrderStatus
    {
        Found = 1,
        NotFound = 2,
        Error = 3,
    }

    class OrderAnswer
    {
        public OrderStatus Status { get; set; }
        public Order Order { get; set; }
        public string ErrorMessage { get; set; }
    }
}
