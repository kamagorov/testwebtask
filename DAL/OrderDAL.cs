﻿using Microsoft.Extensions.Logging;
using System;
using System.Data;
using System.Data.SqlClient;

namespace TestWebMethod.DAL
{
    enum OrderField
    {
        OrderID,
        CustomerID,
        TotalMoney
    }
    class OrderDAL
    {
        private ILogger _logger;
        public readonly string ConnectionString;

        public OrderDAL(ILogger logger, string connectionString)
        {
            _logger = logger;
            ConnectionString = connectionString;
        }

        public Order GetOrderIformation(string orderCode)
        {
            using (IDbConnection connection = GetConnection())
            {
                string query = $"SELECT OrderID, CustomerID, TotalMoney FROM dbo.Orders where OrderCode='{orderCode}'";
                var dataReader = Execute(connection, query);
                if (dataReader.Read())
                {
                    try
                    {
                        var order = new Order(dataReader.GetString((int)OrderField.OrderID), dataReader.GetString((int)OrderField.CustomerID), dataReader.GetDecimal((int)OrderField.TotalMoney));
                        return order;

                    }
                    catch (Exception ex)
                    {
                        _logger.Log("ERROR", $"Cannot get sql with orderCode {orderCode}", ex);
                        throw;
                    }
                }

                return null;
            }
        }

        private IDbConnection GetConnection()
        {
            try
            {
                IDbConnection connection = new SqlConnection(ConnectionString);
                connection.Open();
                return connection;
            }
            catch (InvalidOperationException ioex)
            {
                _logger.Log("ERROR", $"Cannot opend Sql connection. Connection string {ConnectionString}", ioex);
                throw;
            }
            catch (SqlException sqlException)
            {
                _logger.Log("ERROR", $"A connection-level error occurred while opening the connection {ConnectionString}", sqlException);
                throw;
            }
        }

        public IDataReader Execute(IDbConnection connection, string query)
        {
            SqlCommand command = new SqlCommand(query, (SqlConnection)connection);
            connection.Open();
            return command.ExecuteReader();
        }
    }
}
