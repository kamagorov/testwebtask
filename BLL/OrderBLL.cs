﻿using System;
using System.Collections.Concurrent;
using TestWebMethod.DAL;

namespace TestWebMethod.BLL
{
    class OrderBLL
    {
        private readonly ConcurrentDictionary<string, Order> _cache;
        private readonly OrderDAL _orderDAL;

        public OrderBLL(OrderDAL orderDAL)
        {
            _cache = new ConcurrentDictionary<string, Order>();
            _orderDAL = orderDAL;
        }

        public bool TryAddToCache(string key, Order order)
        {
            return _cache.TryAdd(key, order);
        }

        public bool TryRemoveToCache(string key, Order order)
        {
            return _cache.TryRemove(key, out order);
        }

        public Order GetOrderFromCache(string orderCode)
        {
            if (_cache.ContainsKey(orderCode))
            {
                return _cache[orderCode];
            }

            Order order = _orderDAL.GetOrderIformation(orderCode);

            if (order is null)
            {
                return null;
            }

            if (!_cache.ContainsKey(orderCode))
            {
                _cache.TryAdd(orderCode, order);
            }

            return order;
        }
    }
}
