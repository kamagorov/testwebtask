﻿namespace TestWebMethod
{
    public class Order
    {
        public Order(string orderID, string customerID, decimal totalMoney)
        {
            OrderID = orderID;
            CustomerID = customerID;
            TotalMoney = totalMoney;
        }

        public string OrderID { get; set;  }
        public string CustomerID { get; set; }
        public decimal TotalMoney { get; set; }
    }
}